export default function ({ store, redirect }) {
	if (null == store.state.user) {
		redirect('/')
	}
}