const cparser = process.server ? require('cookieparser') : undefined

export const state = () => ({
	user: null
})

export const mutations = {
	set_user(state, user) {
		state.user = user
	},
	clear_user(state) {
		state.user = null
	}
}

export const actions = {
	nuxtServerInit({ commit }, { req }) {
		let user = null
		if (req.headers.cookie) {
			const parsed = cparser.parse(req.headers.cookie)
			try {
				user = parsed.user
			}
			catch (err) {
				console.log(err)
			}
		}
		commit('set_user', user)
	}
}