const express = require('express')
const consola = require('consola')
const bodyParser = require('body-parser')
const { Nuxt, Builder } = require('nuxt')
const { Find } = require('../server/db.js')
const app = express()

const config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
	const nuxt = new Nuxt(config)

	const { host, port } = nuxt.options.server

	if (config.dev) {
		const builder = new Builder(nuxt)
		await builder.build()
	} else {
		await nuxt.ready()
	}

	app.use(bodyParser.json())
	app.use(nuxt.render)

	app.listen(port, host)
	consola.ready({
		message: `Server listening on http://${host}:${port}`,
		badge: true
	})
}

app.get('/login/auth/:user/:pass', (req, res) => {
	const find = new Find('users', { username: req.params.user }, doc => {
		try {
			if (req.params.pass == doc[0].password) {
				res.json({
					auth: true
				})
			}
			else {
				res.json({
					auth: false
				})
			}
		} catch{
			res.json({
				auth: false
			})
		}
	})
	find.execute()
})

start()
