const MongoClient = require('mongodb').MongoClient
const Assert = require('assert')

const url = "mongodb://clqp2008:193755@localhost:27017"
const options = {
    useNewUrlParser: true
}

exports.Find = function (cname, filter, callback) {
    this.client = MongoClient(url, options)
    this.execute = function () {
        this.client.connect(err => {
            Assert.equal(null, err)
            const db = this.client.db('nmms')
            const collection = db.collection(cname)
            collection.find(filter).toArray((err, doc) => {
                Assert.equal(null, err)
                callback(doc)
            })
            this.client.close()
        })
    }
}